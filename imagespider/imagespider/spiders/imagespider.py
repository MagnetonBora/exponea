import scrapy
from scrapy.loader import ItemLoader

from imagespider.items import ImageItem
from imagespider.settings import START_URL


class CoverSpider(scrapy.Spider):
    name = 'exponea-images-crawler'
    start_urls = [START_URL]

    def parse(self, response):
        loader = ItemLoader(item=ImageItem(), response=response)
        loader.add_xpath('image_urls', '//img/@src')
        return loader.load_item()
