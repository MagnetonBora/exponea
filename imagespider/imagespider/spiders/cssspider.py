from collections import defaultdict

import cssutils
import scrapy
from scrapy.loader import ItemLoader

from imagespider.items import CSSItem
from imagespider.settings import START_URL


class CSSSpider(scrapy.Spider):
    name = 'css-crawler'
    start_urls = [START_URL]

    @staticmethod
    def _fetch_css_urls(response):
        loader = ItemLoader(item=CSSItem(), response=response)
        loader.add_xpath('css_urls', '//link[@rel="stylesheet"]/@href')
        items = loader.load_item()
        return [
            item for item in items['css_urls']
        ]

    @staticmethod
    def _fetch_images_urls(url):
        base_url = '/'.join(url.split('/')[:-1])
        stylesheet = cssutils.parseUrl(url)
        img_urls = cssutils.getUrls(stylesheet)
        return [
            '{}/{}'.format(base_url, img_url) for img_url in img_urls
        ]

    def parse(self, response):
        css_urls = self._fetch_css_urls(response)
        result = defaultdict(list)
        for url in css_urls:
            image_urls = self._fetch_images_urls(url)
            result['image_urls'] += image_urls
        return result
