# -*- coding: utf-8 -*-
import logging

from scrapy import Request
from scrapy.exceptions import DropItem
from scrapy.pipelines.images import ImagesPipeline


class ExponeaImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        for image_url in item['image_urls']:
            try:
                yield Request(image_url)
            except Exception as e:
                logging.error('An exceptions has occurred {}'.format(e))
                continue

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]
        if not image_paths:
            raise DropItem('Item contains no images')
        item['image_urls'] = image_paths
        return self.get_images(results, item, info)

    def file_downloaded(self, response, request, info):
        try:
            super().file_downloaded(response, request, info)
        except Exception as e:
            logging.error(
                'An exceptions has occurred when '
                'retrieving {} details: {}'.format(response.url, e)
            )
