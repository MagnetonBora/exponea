# -*- coding: utf-8 -*-
import os

BOT_NAME = 'imagespider'

DEFAULT_START_URL = 'https://exponea.com/'
START_URL = (
   os.environ.get('EXPONEA_IMAGESSPIDER_START_URL') or DEFAULT_START_URL
)

SPIDER_MODULES = ['imagespider.spiders']
NEWSPIDER_MODULE = 'imagespider.spiders'

IMAGES_STORE = 'downloads'
ROBOTSTXT_OBEY = True
CONCURRENT_REQUESTS = 16

ITEM_PIPELINES = {
   'imagespider.pipelines.ExponeaImagesPipeline': 1,
}
