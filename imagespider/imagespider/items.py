# -*- coding: utf-8 -*-

import scrapy


class ImageItem(scrapy.Item):
    images = scrapy.Field()
    image_urls = scrapy.Field()


class CSSItem(scrapy.Item):
    css_urls = scrapy.Field()
