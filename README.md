
This is a simple scrapy-based project to fetch images by the given URL.

To run the program you would need to do the following steps:

1. Create virtual environment (optional).
2. Install requirements `pip install -r requirements.txt`
3. Go to imagespider folder `cd imagespider`
4. Run the crawler by invoking `scrapy crawl exponea-images-crawler` or just
`run.sh` which is alias to `scrapy crawl exponea-images-crawler --loglevel=ERROR`

You should see a new folder *downloads* with images that have been successfully
fetched.

The most important settings of the project are in `imagespider/settings.py`.

You may be interested in changing `START_URL` parameter which is
`https://exponea.com/` by default. To change the value of this parameter without
 changing the content of `settings.py` export `EXPONEA_IMAGESSPIDER_START_URL`
 environment variable.

Example: `export EXPONEA_IMAGESSPIDER_START_URL=https://www.petfinder.com`.
